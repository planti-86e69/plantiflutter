import 'package:firebase_auth/firebase_auth.dart';

import 'package:plantiflutter/src/models/plantiuser.dart';
import 'package:plantiflutter/src/services/database_service.dart';

class AuthenticationService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Create PlantiUser object from Firebase User
  PlantiUser? _plantiUserFromFirebaseUser(User? user) {
    return user != null ? PlantiUser(uid: user.uid) : null;
  }

  // auth change user stream (returns null or PlantiUser object)
  Stream<PlantiUser?> get user {
    return _auth.userChanges()
        //.map((User? user) => _plantiUserFromFirebaseUser(user));
        // or equivalently,
        .map(_plantiUserFromFirebaseUser);
  }

  // Register with email and password
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      User? user = result.user;
      
      // create a new document in firestore for the user with the uid
      await DatabaseService(uid: user!.uid).updateUserData("New Planti member", email);  // userCollection
      await DatabaseService(uid: user.uid).updateUserPlantsData("My plant", 0, null);  //plantsCollection

      return _plantiUserFromFirebaseUser(user);
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

  // Sign in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      User? user = result.user;
      return _plantiUserFromFirebaseUser(user);
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

  // Sign in anonymously
  Future signInAnon() async {
    try {
      UserCredential result = await _auth.signInAnonymously();
      User? user = result.user;
      return _plantiUserFromFirebaseUser(user);
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

  // Sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

}