import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService{
  // collection references (if they don't exist yet, they're created automatically)
  final CollectionReference userCollection = FirebaseFirestore.instance.collection("userCollection");
  final CollectionReference plantsCollection = FirebaseFirestore.instance.collection("plantsCollection");
  
  final String? uid;
  DatabaseService({this.uid});

  Future updateUserData(String name, String email) async {
    return await userCollection.doc(uid).set({
      // 'uniquePlantID': uniquePlantID, // create unique plant identifier
      'name': name,
      'email': email
    });
  }

  Future updateUserPlantsData(String name, double age, String? pictureSource) async {
    return await plantsCollection.doc(uid).set({
      // 'uniquePlantID': uniquePlantID, // create unique plant identifier
      'name': name,
      'age': age,
      'pictureSource': pictureSource
    });
  }

  // get user stream
  Stream<QuerySnapshot> get userStream {
    return userCollection.snapshots();
  }

  // get plants stream
  Stream<QuerySnapshot> get plantsStream {
    return plantsCollection.snapshots();
  }
}