import 'package:flutter/material.dart';

class Plant {
  //final String uniquePlantId;  // generate at instantiation, should be unique per user
  String? name;  // user-friendly distinction of plants (in case of multiple plants of same kind)
  final double? age;
  Image? pictureSource;  // should contain path to Image that is chosen from the gallery or created from the camera

  Plant({this.name, this.age, this.pictureSource});
}

