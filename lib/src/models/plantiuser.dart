class PlantiUser {
  final String uid;
  String? name = "New Planti member";
  String? email = "nomail";

  PlantiUser({required this.uid, this.name, this.email});
}