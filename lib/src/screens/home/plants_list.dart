import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';

class PlantsList extends StatefulWidget {
  const PlantsList({Key? key}) : super(key: key);

  @override
  _PlantsListState createState() => _PlantsListState();
}

class _PlantsListState extends State<PlantsList> {
  @override
  Widget build(BuildContext context) {
    final plants = Provider.of<QuerySnapshot?>(context);
    //print(plants.docs);
    for (var doc in plants!.docs) {
      print(doc.data());
    }
    return Container();
  }
}
