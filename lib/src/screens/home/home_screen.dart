import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'package:plantiflutter/src/services/authentication_service.dart';
import 'package:plantiflutter/src/services/database_service.dart';
import 'package:plantiflutter/src/screens/home/plants_list.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  final AuthenticationService _auth = AuthenticationService();
  final List<XFile?> _plants = [];
  late XFile? imageFile;
  late String tst;

  _openGallery(BuildContext context) async {
    XFile? picture = await ImagePicker().pickImage(source: ImageSource.gallery);
    setState(() {
      imageFile = picture;
      tst = "Hallo Elena";
    });
    Navigator.of(context).pop(); // close dialog
  }

  _openCamera(BuildContext context) async {
    XFile? picture = await ImagePicker().pickImage(source: ImageSource.camera);
    setState(() {
      imageFile = picture;
    });
    Navigator.of(context).pop(); // close dialog
  }

  Future<void> _showDialogImageInput(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: const Text('Choose input source for new image'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    GestureDetector(
                        child: const Text('Gallery'),
                        onTap: () {
                          _openGallery(context);
                        }),
                    const Padding(padding: EdgeInsets.all(8.0)),
                    GestureDetector(
                      child: const Text('Camera'),
                      onTap: () {
                        _openCamera(context);
                      },
                    ),
                  ],
                ),
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    return StreamProvider<QuerySnapshot?>.value(
        initialData: null,
        value: DatabaseService().plantsStream,
        child: Scaffold(
            backgroundColor: Colors.green[300],
            appBar: AppBar(
              backgroundColor: Colors.green[900],
              elevation: 0.0,
              title: const Text('My Plants'),
              actions: <Widget>[
                TextButton.icon(
                  icon: const Icon(Icons.person),
                  label: const Text('Logout'),
                  onPressed: () async {
                    await _auth.signOut();
                  },
                )
              ],
            ),
            body: const PlantsList()
            /*body: Center(
                child: Column(children: [
              Container(
                  margin: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() async {
                        await _showDialogImageInput(context);
                        _plants.add(imageFile);
                      });
                    },
                    child: const Text('Add Plant'),
                  )),
              Column(
                  children: _plants
                      .map((element) => Card(
                              child: Column(children: <Widget>[
                            Text(tst)
                            //Image.asset('assets/monstera.jpg')  // This should display the old images in _plants. HOW???
                            //Image.asset(this._plants)  // This should display the old images in _plants. HOW???
                          ])))
                      .toList())
            ]))*/
        ));
  }
}
