import 'package:flutter/material.dart';
import 'package:plantiflutter/src/models/plantiuser.dart';
import 'package:plantiflutter/src/screens/home/home_screen.dart';
import 'package:plantiflutter/src/screens/login/login_screen.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<PlantiUser?>(context);
    // return either Home or Login widget
    if (user == null) {
      return const LoginScreen();
    } else {
      return const HomeScreen();
    }
  }
}