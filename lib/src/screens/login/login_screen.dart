import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:plantiflutter/src/screens/login/sign_in.dart';
import 'package:plantiflutter/src/screens/login/sign_in_for_testing.dart';
import 'package:plantiflutter/src/screens/login/register.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<LoginScreen> {
  bool showSignIn = true;

  void toggleView() {
    setState(() => showSignIn = !showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return SignIn(toggleView);  // choose SignInForTesting() for having anonymous sign in, otherwise choose SignIn()
    } else {
      return Register(toggleView);
    }
  }
}
