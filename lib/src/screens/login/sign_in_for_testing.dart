import 'package:flutter/material.dart';
import 'package:plantiflutter/src/services/authentication_service.dart';
import 'package:plantiflutter/src/shared/loading.dart';

class SignInForTesting extends StatefulWidget {
  final Function toggleView;

  SignInForTesting(this.toggleView);

  @override
  _SignInForTestingState createState() => _SignInForTestingState();
}

class _SignInForTestingState extends State<SignInForTesting> {
  final AuthenticationService _auth = AuthenticationService();
  bool loading = false;

  // TextFormField state
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading
        ? const Loading()
        : Scaffold(
            backgroundColor: Colors.brown[100],
            appBar: AppBar(
                backgroundColor: Colors.brown[400],
                elevation: 0.0,
                title: const Text('Sign in to Planti'),
                actions: <Widget>[
                  TextButton.icon(
                    icon: const Icon(Icons.person),
                    label: const Text('Register'),
                    onPressed: () {
                      widget.toggleView();
                    },
                  )
                ]),
            body: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 50.0),
                child: Center(
                    child: Column(children: <Widget>[
                  ElevatedButton(
                    child: const Text('Sign in anonymously'),
                    onPressed: () async {
                      setState(() => loading = true);
                      dynamic result = await _auth.signInAnon();
                      if (result == null) {
                        setState(() {
                          error =
                              'Could not sign in. Consider registering to Planti.';
                          loading = false;
                        });
                      } // else a User is returned, and therefore the wrapper leads to showing the HomeScreen
                    },
                  ),
                  const SizedBox(height: 20.0),
                  Text(error,
                      style: const TextStyle(color: Colors.red, fontSize: 14.0))
                ]))));
  }
}
