import 'package:flutter/material.dart';

import 'package:plantiflutter/src/services/authentication_service.dart';
import 'package:plantiflutter/src/shared/constants.dart';
import 'package:plantiflutter/src/shared/loading.dart';

class SignIn extends StatefulWidget {

  final Function toggleView;
  SignIn(this.toggleView);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthenticationService _auth = AuthenticationService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  // TextFormField state
  String _email = '';
  String _password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading ? const Loading() : Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: const Text('Sign in to Planti'),
        actions: <Widget>[
          TextButton.icon(
            icon: const Icon(Icons.person),
            label: const Text('Register'),
            onPressed: () {
              widget.toggleView();
            },
          )
        ]
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
            key: _formKey,
            child: Column(children: <Widget>[
              const SizedBox(height: 20.0),
              TextFormField(
                  decoration: textInputDecoration.copyWith(hintText: 'Email'),
                  validator: (val) => val!.isEmpty ? 'Enter an email' : null,
                  onChanged: (val) {
                    setState(() => _email = val);
                  }),
              const SizedBox(height: 20.0),
              TextFormField(
                  decoration: textInputDecoration.copyWith(hintText: 'Password'),
                  obscureText: true,
                  validator: (val) => val!.length < 6 ? 'Enter a password of at least 6 characters' : null,
                  onChanged: (val) {
                    setState(() => _password = val.trim());
                  }),
              const SizedBox(height: 20.0),
              ElevatedButton(
                child: const Text('Sign in', style: TextStyle(color: Colors.white)),
                onPressed: () async {
                  // only validates to true if both validators return null
                  if (_formKey.currentState!.validate()) {
                    setState(() => loading = true);
                    dynamic result = await _auth.signInWithEmailAndPassword(_email, _password);
                    // check firebase validations for supplied email
                    if (result == null) {
                      setState(() {
                        error = 'Could not sign in. Consider registering to Planti.';
                        loading = false;
                      });
                    } // else a User is returned, and therefore the wrapper leads to showing the HomeScreen
                  }
                }
                ),
              const SizedBox(height: 20.0),
              Text(error, style: const TextStyle(color: Colors.red, fontSize: 14.0))
            ]
            )
        ),
      ),
    );
  }
}
