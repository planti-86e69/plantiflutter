import 'package:flutter/material.dart';
import 'package:plantiflutter/src/models/plantiuser.dart';
import 'package:provider/provider.dart';

import 'package:plantiflutter/src/screens/wrapper.dart';
import 'package:plantiflutter/src/services/authentication_service.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamProvider<PlantiUser?>.value(
      initialData: null,
      value: AuthenticationService().user,
      child: const MaterialApp(
        title: 'Planti',
        home: Wrapper(),
      ),
    );
  }
}
